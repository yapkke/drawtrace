#!/usr/bin/env python
import sys


class tr_file:
    def __init__(self, filename):
        self.nodes = []

        fileRef = open(x, "r")
        for line in fileRef:
            res = self.parse_line(line)
            if (res != None):
                if (res[0] == None):
                    self.nodes.append(None)
                else:
                    self.nodes.append(res)
        fileRef.close()
        self._give_names()

    def parse_line(self, line):
        items = line.split()
        if (items[0] == "traceroute"):
            return None
        
        name = None
        ipaddr = None
        for i in items[1:]:
            if (i != "*"):
                if (name == None):
                    name = i
                elif (ipaddr == None):
                    ipaddr = i
                    return (name, ipaddr)
        return (None, None)

    def _give_names(self):
        res = []
        for i in range(0, len(self.nodes)):
            if (self.nodes[i] == None):
                if ((i != 0) and (i != len(self.nodes)-1)):
                    p = self._get_prev_named(i)
                    n =  self._get_next_named(i)
                    res.append(("",
                                p[1][1]+n[1][1]+str(p[0]-1).strip()))
            else:
                res.append(self.nodes[i])
        self.nodes = res[:]

    def _get_prev_named(self, i):
        c = 0
        while (self.nodes[i] == None) and (i > 0):
            i -= 1
            c += 1
        return (c, self.nodes[i])

    def _get_next_named(self, i):
        c = 0
        while (self.nodes[i] == None) and (i < len(self.nodes)-1):
            i += 1
            c += 1
        return (c, self.nodes[i])
            

    def get_route(self):
        index = 0
        for i in range(0,len(self.nodes)):
            if ((self.nodes[i] != None) and 
                (self.nodes[i][0][-12:] == "stanford.edu")):
                index = i
                break
        if (index > 0):
            index -= 1

        r = []
        for n in self.nodes[index:]:
            r.append(n)
        return r

class network:
    def __init__(self):
        self.nodes = []
        self.node_names = []
        self.end_node = []
        self.edges = []

    def add_route(self, r):
        for i in range(0, len(r)):
            if (r[i][1] not in self.nodes):
                if (r[i][1] not in self.nodes) and (i != 0):
                    self.edges.append((r[i-1][1],r[i][1]))
                if (i != len(r)-1):
                    if (r[i+1][1] in self.nodes):
                        self.edges.append((r[i][1],r[i+1][1]))

                self.nodes.append(r[i][1])
                self.node_names.append(r[i][0])
                self.end_node.append((i == len(r)-1))

    def get_dot(self, more=None):
        res = "digraph {\n"
        for e in self.edges:
            res+="\t\""+e[0]+"\"->\""+e[1]+"\";\n"

        res+="\n"

        for i in range(0, len(self.nodes)):
            nname = self.node_names[i]
            if (nname != ""):
                nname += " "+self.nodes[i]
            elif (not self.end_node[i]):
                res+= "\t\""+self.nodes[i]+"\" [style=filled,fillcolor=yellow,label=\""+nname+"\"];\n"
            else:
                res+= "\t\""+self.nodes[i]+"\" [label=\""+self.node_names[i]+"\"];\n"

        if (more != None):
            fileRef = open(more, "r")
            for l in fileRef:
                res += l
            fileRef.close()
                
        res += "}"
        return res
        
n = network()
for x in sys.argv[1:]:
    n.add_route(tr_file(x).get_route())

fileRef = open("output.dot", "w")
fileRef.write(n.get_dot("more.dot"))
fileRef.close()
